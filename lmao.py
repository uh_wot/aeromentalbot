import os
import logging
from telegram import Update
from telegram.ext import Updater, MessageHandler
from telegram.ext.filters import Filters

logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                     level=logging.INFO)

TOKEN = os.environ.get("TOKEN")
# webhooks
URL = os.environ.get("URL")
PORT = os.environ.get("PORT")
CERT_PATH = os.environ.get("CERT_PATH")

updater = Updater(TOKEN, use_context=True)
dp = updater.dispatcher


if URL:
    updater.start_webhook(listen="0.0.0.0",
                          port=int(PORT),
                          url_path=TOKEN)
    if CERT_PATH:
        logging.info("Webhooks with HTTPS certificate enabled.")
        updater.bot.set_webhook(URL + TOKEN,
                                certificate=open(CERT_PATH, "rb"))

    else:
        logging.info("Webhooks without HTTPS certificate enabled.")
        updater.bot.set_webhook(URL + TOKEN)

else:
    logging.info("Webhooks disabled, using long polling instead.")


def lmao(update: Update, _):
    update.effective_message.reply_text("Lmao.")
    logging.info("lmao")


dp.add_handler(MessageHandler(Filters.all, lmao, run_async=True))

if not URL:
    updater.start_polling()

logging.info("Started.")
updater.idle()
